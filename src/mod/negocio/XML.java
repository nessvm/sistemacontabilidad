package mod.negocio;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import javax.sql.DataSource;

import mod.dao.DocumentDAO;
import mod.dao.GenericCRUD;
import mod.documentos.Cuenta;
import mod.documentos.Subcuenta;
import mod.usuario.Empresa;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class XML {
	private OutputStream xml_file;
	private ArrayList<String> cuentasClasif, subCtas;
	private DataSource ds;
	private double sumCirc, sumNoCirc, sumCorto, sumLargo, sumCC, sumComp, sumDeud, sumAcree;

	public XML(DataSource ds, OutputStream out) {
		this.xml_file = (FileOutputStream) out;
		this.ds = ds;
		this.cuentasClasif = new ArrayList<>();
		this.subCtas = new ArrayList<>();
		sumCirc = sumNoCirc = sumCorto = sumLargo = sumCC = sumComp = sumDeud = sumAcree = 0;
	}        

	public void doXML(Empresa empresa) {
		Cuenta cta;
		Document doc = new Document();
		Calendar fecha = Calendar.getInstance();
		DocumentDAO docDao = new DocumentDAO(ds);
		GenericCRUD keeper = new GenericCRUD(ds);
		
		try {
			Element raiz = new Element("Estado");
			raiz.setAttribute("empresa", empresa.getRazonS());
			raiz.setAttribute("RFC", empresa.getRfc());
			raiz.setAttribute("dia", String.valueOf(fecha.get(Calendar.DAY_OF_MONTH)));
			raiz.setAttribute("mes", getMes(fecha.get(Calendar.MONTH)));
			raiz.setAttribute("a�o", String.valueOf(fecha.get(Calendar.YEAR)));

			Element circulante = new Element("Circulante");
			Element noCirculante = new Element("NoCirculante");
			Element cortoPlazo = new Element("CortoPlazo");
			Element largoPlazo = new Element("LargoPlazo");
			Element capital = new Element("CapitalContable");
			Element comActivo = new Element("ComplementoActivo");
			Element resDeudoras = new Element("ResultadosDeudoras");
			Element redAcreedoras = new Element("ResultadosAcreedoras");

			for(int i = 0; i < getNumCuentas("Circulante"); i++){
				Element cuenta = new Element("Cuenta");
				cta = (Cuenta)keeper.selectObject(cuentasClasif.get(i), "Cuenta");
				cuenta.setAttribute("id", String.valueOf(cta.getIdCta()));
				cuenta.setAttribute("nombre", cta.getNombre());
				cuenta.setAttribute("saldo", String.valueOf(cta.getSaldo()));
				subCtas = docDao.getSubAccountList();
				System.out.println("Cta");
				for (int j = 0; j < subCtas.size(); j++){
					System.out.println(subCtas.get(j));
					Subcuenta sub = (Subcuenta)keeper.selectObject(subCtas.get(j), "Subcuenta");
					if (sub.getIdCuenta() == cta.getIdCta()){
						Element subcuenta = new Element("Subcuenta");
						subcuenta.setAttribute("id", String.valueOf(sub.getIdSubcta()));
						subcuenta.setAttribute("nombre", sub.getNombre());
						subcuenta.setAttribute("saldo",String.valueOf(sub.getSaldo()));
						cuenta.addContent(subcuenta);
						System.out.println("Sub");
					}
				}
				sumCirc += cta.getSaldo();
				circulante.addContent(cuenta);
			}

			for(int i = 0; i < getNumCuentas("NoCirculante"); i++){
				Element cuenta = new Element("Cuenta");
				cta = (Cuenta)keeper.selectObject(cuentasClasif.get(i), "Cuenta");
				cuenta.setAttribute("id", String.valueOf(cta.getIdCta()));
				cuenta.setAttribute("nombre", cta.getNombre());
				cuenta.setAttribute("saldo", String.valueOf(cta.getSaldo()));
				subCtas = docDao.getSubAccountList();
				for (int j = 0; j < subCtas.size(); j++){
					Subcuenta sub = (Subcuenta)keeper.selectObject(subCtas.get(j), "Subcuenta");
					if (sub.getIdCuenta() == cta.getIdCta()){
						Element subcuenta = new Element("Subcuenta");
						subcuenta.setAttribute("id", String.valueOf(sub.getIdSubcta()));
						subcuenta.setAttribute("nombre", sub.getNombre());
						subcuenta.setAttribute("saldo",String.valueOf(sub.getSaldo()));
						cuenta.addContent(subcuenta);
					}
				}
				sumNoCirc += cta.getSaldo();
				noCirculante.addContent(cuenta);
			}

			for(int i = 0; i < getNumCuentas("CortoPlazo"); i++){
				Element cuenta = new Element("Cuenta");
				cta = (Cuenta)keeper.selectObject(cuentasClasif.get(i), "Cuenta");
				cuenta.setAttribute("id", String.valueOf(cta.getIdCta()));
				cuenta.setAttribute("nombre", cta.getNombre());
				cuenta.setAttribute("saldo", String.valueOf(cta.getSaldo()));
				subCtas = docDao.getSubAccountList();
				for (int j = 0; j < subCtas.size(); j++){
					Subcuenta sub = (Subcuenta)keeper.selectObject(subCtas.get(j), "Subcuenta");
					if (sub.getIdCuenta() == cta.getIdCta()){
						Element subcuenta = new Element("Subcuenta");
						subcuenta.setAttribute("id", String.valueOf(sub.getIdSubcta()));
						subcuenta.setAttribute("nombre", sub.getNombre());
						subcuenta.setAttribute("saldo",String.valueOf(sub.getSaldo()));
						cuenta.addContent(subcuenta);
					}
				}
				sumCorto += cta.getSaldo();
				cortoPlazo.addContent(cuenta);
			}


			for(int i = 0; i < getNumCuentas("LargoPlazo"); i++){
				Element cuenta = new Element("Cuenta");
				cta = (Cuenta)keeper.selectObject(cuentasClasif.get(i), "Cuenta");
				cuenta.setAttribute("id", String.valueOf(cta.getIdCta()));
				cuenta.setAttribute("nombre", cta.getNombre());
				cuenta.setAttribute("saldo", String.valueOf(cta.getSaldo()));
				subCtas = docDao.getSubAccountList();
				for (int j = 0; j < subCtas.size(); j++){
					Subcuenta sub = (Subcuenta)keeper.selectObject(subCtas.get(j), "Subcuenta");
					if (sub.getIdCuenta() == cta.getIdCta()){
						Element subcuenta = new Element("Subcuenta");
						subcuenta.setAttribute("id", String.valueOf(sub.getIdSubcta()));
						subcuenta.setAttribute("nombre", sub.getNombre());
						subcuenta.setAttribute("saldo",String.valueOf(sub.getSaldo()));
						cuenta.addContent(subcuenta);
					}
				}
				sumLargo += cta.getSaldo();
				largoPlazo.addContent(cuenta);
			}


			for(int i = 0; i < getNumCuentas("Capital"); i++){
				Element cuenta = new Element("Cuenta");
				cta = (Cuenta)keeper.selectObject(cuentasClasif.get(i), "Cuenta");
				cuenta.setAttribute("id", String.valueOf(cta.getIdCta()));
				cuenta.setAttribute("nombre", cta.getNombre());
				cuenta.setAttribute("saldo", String.valueOf(cta.getSaldo()));
				subCtas = docDao.getSubAccountList();
				for (int j = 0; j < subCtas.size(); j++){
					Subcuenta sub = (Subcuenta)keeper.selectObject(subCtas.get(j), "Subcuenta");
					if (sub.getIdCuenta() == cta.getIdCta()){
						Element subcuenta = new Element("Subcuenta");
						subcuenta.setAttribute("id", String.valueOf(sub.getIdSubcta()));
						subcuenta.setAttribute("nombre", sub.getNombre());
						subcuenta.setAttribute("saldo",String.valueOf(sub.getSaldo()));
						cuenta.addContent(subcuenta);
					}
				}
				sumCC += cta.getSaldo();
				capital.addContent(cuenta);
			}


			for(int i = 0; i < getNumCuentas("ComplementoActivo"); i++){
				Element cuenta = new Element("Cuenta");
				cta = (Cuenta)keeper.selectObject(cuentasClasif.get(i), "Cuenta");
				cuenta.setAttribute("id", String.valueOf(cta.getIdCta()));
				cuenta.setAttribute("nombre", cta.getNombre());
				cuenta.setAttribute("saldo", String.valueOf(cta.getSaldo()));
				subCtas = docDao.getSubAccountList();
				for (int j = 0; j < subCtas.size(); j++){
					Subcuenta sub = (Subcuenta)keeper.selectObject(subCtas.get(j), "Subcuenta");
					if (sub.getIdCuenta() == cta.getIdCta()){
						Element subcuenta = new Element("Subcuenta");
						subcuenta.setAttribute("id", String.valueOf(sub.getIdSubcta()));
						subcuenta.setAttribute("nombre", sub.getNombre());
						subcuenta.setAttribute("saldo",String.valueOf(sub.getSaldo()));
						cuenta.addContent(subcuenta);
					}
				}
				sumComp += cta.getSaldo();
				comActivo.addContent(cuenta);
			}
			
			for(int i = 0; i < getNumCuentas("Deudor"); i++){
				Element cuenta = new Element("Cuenta");
				cta = (Cuenta)keeper.selectObject(cuentasClasif.get(i), "Cuenta");
				cuenta.setAttribute("id", String.valueOf(cta.getIdCta()));
				cuenta.setAttribute("nombre", cta.getNombre());
				cuenta.setAttribute("saldo", String.valueOf(cta.getSaldo()));
				subCtas = docDao.getSubAccountList();
				for (int j = 0; j < subCtas.size(); j++){
					Subcuenta sub = (Subcuenta)keeper.selectObject(subCtas.get(j), "Subcuenta");
					if (sub.getIdCuenta() == cta.getIdCta()){
						Element subcuenta = new Element("Subcuenta");
						subcuenta.setAttribute("id", String.valueOf(sub.getIdSubcta()));
						subcuenta.setAttribute("nombre", sub.getNombre());
						subcuenta.setAttribute("saldo",String.valueOf(sub.getSaldo()));
						cuenta.addContent(subcuenta);
					}
				}
				sumDeud += cta.getSaldo();
				resDeudoras.addContent(cuenta);
			}
			
			for(int i = 0; i < getNumCuentas("Acreedor"); i++){
				Element cuenta = new Element("Cuenta");
				cta = (Cuenta)keeper.selectObject(cuentasClasif.get(i), "Cuenta");
				cuenta.setAttribute("id", String.valueOf(cta.getIdCta()));
				cuenta.setAttribute("nombre", cta.getNombre());
				cuenta.setAttribute("saldo", String.valueOf(cta.getSaldo()));
				subCtas = docDao.getSubAccountList();
				for (int j = 0; j < subCtas.size(); j++){
					Subcuenta sub = (Subcuenta)keeper.selectObject(subCtas.get(j), "Subcuenta");
					if (sub.getIdCuenta() == cta.getIdCta()){
						Element subcuenta = new Element("Subcuenta");
						subcuenta.setAttribute("id", String.valueOf(sub.getIdSubcta()));
						subcuenta.setAttribute("nombre", sub.getNombre());
						subcuenta.setAttribute("saldo",String.valueOf(sub.getSaldo()));
						cuenta.addContent(subcuenta);
					}
				}
				sumAcree += cta.getSaldo();
				redAcreedoras.addContent(cuenta);
			}

			Element activo = new Element("Activo");
			circulante.setAttribute("suma", String.valueOf(sumCirc));
			activo.addContent(circulante);
			noCirculante.setAttribute("suma", String.valueOf(sumNoCirc));
			activo.addContent(noCirculante);
			activo.setAttribute("suma", String.valueOf(sumCirc + sumNoCirc));
			Element pasivo = new Element("Pasivo");
			cortoPlazo.setAttribute("suma", String.valueOf(sumCorto));
			pasivo.addContent(cortoPlazo);
			largoPlazo.setAttribute("suma", String.valueOf(sumLargo));
			pasivo.addContent(largoPlazo);
			pasivo.setAttribute("suma", String.valueOf(sumLargo + sumCorto));
			capital.setAttribute("suma", String.valueOf(sumCC));
			comActivo.setAttribute("suma", String.valueOf(sumComp));
			resDeudoras.setAttribute("suma", String.valueOf(sumDeud));
			redAcreedoras.setAttribute("suma", String.valueOf(sumAcree));
			raiz.addContent(activo);
			raiz.addContent(pasivo);
			raiz.addContent(capital);
			raiz.addContent(comActivo);
			raiz.addContent(resDeudoras);
			raiz.addContent(redAcreedoras);
			
			doc.setRootElement(raiz);

			XMLOutputter outxml = new XMLOutputter(Format.getPrettyFormat());
			outxml.output(doc, xml_file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Obtiene el n�mero de cuentas que hay en la base de datos
	 * @param subClasificacion Una cadena con la clasificaci�n o subclasificaci�n de la cuenta, ej. "Activo", "Pasivo", "CortoPlazo"
	 * @return int con el n�mero de cuentas en la base
	 */
	private int getNumCuentas(String subClasificacion){
		DocumentDAO docDao = new DocumentDAO(ds);
		ArrayList<String> lista;
		String auxCompare;
		int count = 0;
		
		switch(subClasificacion){

		case "Circulante":
			lista = docDao.getAccountList();
			for (int i = 0; i < lista.size(); i++){
				auxCompare = lista.get(i);
				if (auxCompare.charAt(0) == '1' && auxCompare.charAt(1) == '1'){
					cuentasClasif.add(auxCompare);
					count++;
					continue;
				}	
			}
			return count;

		case "NoCirculante":
			lista = docDao.getAccountList();
			for (int i = 0; i < lista.size(); i++){
				auxCompare = lista.get(i);
				if (auxCompare.charAt(0) == '1' && auxCompare.charAt(1) == '2'){
					count++;
					continue;
				}	
			}
			return count;
			
		case "CortoPlazo":
			lista = docDao.getAccountList();
			for (int i = 0; i < lista.size(); i++){
				auxCompare = lista.get(i);
				if (auxCompare.charAt(0) == '2' && auxCompare.charAt(1) == '1'){
					count++;
					continue;
				}	
			}
			return count;
			
		case "LargoPlazo":
			lista = docDao.getAccountList();
			for (int i = 0; i < lista.size(); i++){
				auxCompare = lista.get(i);
				if (auxCompare.charAt(0) == '2' && auxCompare.charAt(1) == '2'){
					count++;
					continue;
				}	
			}
			return count;
		
		case "Capital":
			lista = docDao.getAccountList();
			for (int i = 0; i < lista.size(); i++){
				auxCompare = lista.get(i);
				if (auxCompare.charAt(0) == '3'){
					count++;
					continue;
				}	
			}
			return count;
		
		case "ComplementoActivo":
			lista = docDao.getAccountList();
			for (int i = 0; i < lista.size(); i++){
				auxCompare = lista.get(i);
				if (auxCompare.charAt(0) == '4'){
					count++;
					continue;
				}	
			}
			return count;
		
		case "Deudor":
			lista = docDao.getAccountList();
			for (int i = 0; i < lista.size(); i++){
				auxCompare = lista.get(i);
				if (auxCompare.charAt(0) == '5'){
					count++;
					continue;
				}	
			}
			return count;
			
		case "Acreedor":
			lista = docDao.getAccountList();
			for (int i = 0; i < lista.size(); i++){
				auxCompare = lista.get(i);
				if (auxCompare.charAt(0) == '6'){
					count++;
					continue;
				}	
			}
			return count;
		}
		
		return 0;
	}
	
	private static String getMes(int month){
		if (month == 0)
			return "Enero";
		
		else if (month == 1)
			return "Febrero";
		
		else if (month == 2)
			return "Marzo";
		
		else if (month == 3)
			return "Abril";
		
		else if (month == 4)
			return "Mayo";
		
		else if (month == 5)
			return "Junio";
		
		else if (month == 6)
			return "Julio";
		
		else if (month == 7)
			return "Agosto";
		
		else if (month == 8)
			return "Septiembre";
		
		else if (month == 9)
			return "Octubre";
		
		else if (month == 10)
			return "Noviembre";
		
		else if (month == 11)
			return "Diciembre";
		
		else
			return null;
	}
}
