package mod.negocio;

import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import mod.documentos.Movimiento;
import mod.documentos.Poliza;
import mod.usuario.Empresa;
import mod.usuario.Usuario;

public class RequestFetcher {

	public RequestFetcher() {
		super();
	}

	/**
	 * @param request
	 *            del Servlet que invoca.
	 * @return mod.usuario.Usuario con los atributos inicializados a excepci�n
	 *         de idUsr y tipoU.
	 */
	public static Usuario getUserParameters(HttpServletRequest request) {
		Usuario user;

		user = new Usuario();
		user.setNombre(request.getParameter("txtName"));
		user.setApPat(request.getParameter("txtSurname"));
		user.setApMat(request.getParameter("txtMatSurname"));
		Usuario enSesion = (Usuario) request.getSession().getAttribute(
				"usuario");
		if (enSesion.getTipoU().equals("Administrador"))
			user.setTipoU(request.getParameter("lstTypeUser"));
		else
			user.setTipoU("Contador");
		if (request.getParameter("txtPassword").equals(
				request.getParameter("txtPassword1")))
			user.setContrase�a(request.getParameter("txtPassword"));
		else {
			System.out.println("Las contrase�as no coinciden");
			return null;
		}

		if (request.getParameter("txtEmail").equals(
				request.getParameter("txtEmail1")))
			user.setEmail(request.getParameter("txtEmail"));
		else {
			System.out.println("Los correos no coinciden");
			return null;
		}
		return user;
	}

	/**
	 * @param request
	 *            del Servlet que invoca.
	 * @return mod.documentos.Poliza con los atributos inicializados a excepci�n
	 *         de numP.
	 */
	public static Poliza getPolizaParameters(HttpServletRequest request) {
		Poliza pol;
		Empresa com;
		String fecha;

		if ((com = (Empresa) request.getSession().getAttribute("empresa")) == null) {
			System.out.println("No existe una empresa en sesi�n");
			return null;
		}

		pol = new Poliza();
		pol.setTipo(request.getParameter("lstTypeP"));
		pol.setRfc(request.getParameter("txtRFC"));
		pol.setFecha(Calendar.getInstance());
		pol.setDescripcion(request.getParameter("txtDescrP"));
		pol.setRfc(com.getRfc());
		fecha = request.getParameter("txtDate");
		pol.setFecha(Integer.parseInt(fecha.substring(0, 2)),
				Integer.parseInt(fecha.substring(3, 5)),
				Integer.parseInt(fecha.substring(6)));
		return pol;
	}

	/**
	 * 
	 * @param request
	 *            HttpServletRequest del servlet que invoca
	 * @param raiz
	 *            poliza que contendr� los movimientos de la p�liza
	 * @return ArrayList<> con los movimientos descritos para la poliza ra�z
	 *         especificada, menos identificadores.
	 */
	public static ArrayList<Movimiento> getAllMovParameters(
			HttpServletRequest request, Poliza raiz) {
		int i = 0;
		double importe;
		ArrayList<Movimiento> movs;

		movs = new ArrayList<>();
		do {
			movs.add(new Movimiento());
			movs.get(i).setNumP(raiz.getNumero());
			movs.get(i).setTipoP(raiz.getTipo());
			movs.get(i).setFecha(raiz.getFechaAsCalendar());
			movs.get(i).setRfc(raiz.getRfc());
			movs.get(i).setIdCuenta(request.getParameter("txtAccountN" + (i+1)));
			movs.get(i).setConcepto(
					request.getParameter("txtConcept" + (i + 1)));
			if ((importe = Double.parseDouble(request.getParameter("txtDebe"
					+ (i + 1)))) != 0) {
				movs.get(i).setImporte(-importe);
			} else {
				movs.get(i).setImporte(
						Double.parseDouble(request.getParameter("txtHaber"
								+ (i + 1))));
			}
			i++;
		} while (request.getParameter("txtConcept" + (i + 1)) != null
				&& request.getParameter("txtAccountN" + (i + 1)) != null);

		return movs;
	}
}
