package mod.negocio;

import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class StreamDocument {

	public StreamDocument() {
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * Este m�todo crea un transformador con la hoja de estilo del flujo xslStream, para
	 * 		despu�s transformar el documento del flujo xmlStream en una p�gina HTML que 
	 * 		es enviada al flujo de salida htmlStream
	 * @param xmlStream
	 * 		El flujo de bytes le�dos del archivo XML dentro de WebContent que el usuario
	 * 		dirija en la p�gina principal.
	 * @param xslStream
	 * 		El flujo de bytes le�dos de la hoja de estilo XSL dentro de WebContent que el
	 * 		usuario dirija en la p�gina principal.
	 * @param htmlStream
	 * 		El flujo de salida tomado con el m�todo getOutputStream() del servlet, al es-
	 * 		cribir aqu� con el m�todo transform, el resultado se ver� reflejado inmedia-
	 * 		tamente en el navegador.
	 */
	public static void transform(InputStream xmlStream, InputStream xslStream, OutputStream htmlStream){
		try{
			TransformerFactory factory = TransformerFactory.newInstance();
			StreamSource xslSource = new StreamSource(xslStream);
			StreamSource xmlSource = new StreamSource(xmlStream);
			StreamResult htmlResult = new StreamResult(htmlStream);
			
			Transformer morpher = factory.newTransformer(xslSource);
			morpher.transform(xmlSource, htmlResult);
			
		} catch(TransformerConfigurationException e){
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

}
