/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mod.documentos;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Alan
 */
public class Poliza {
    private int numero;
    private String tipo;
    private String descripcion;
    private String rfc;
    private Calendar fecha;

    public Poliza()
    {
        
    }
    
    public Poliza(int numero, String tipo, String descripcion, String rfc)
    {
        this.numero = numero;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.rfc = rfc;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getNumero() {
        return numero;
    }

    public String getRfc() {
        return rfc;
    }

    public String getTipo() {
        return tipo;
    }

    public void setFecha(int a�o, int mes, int dia) {//En caso de que se quiere asignar una fecha especifica
       fecha = Calendar.getInstance();
       fecha.set(a�o,mes-1,dia);
    }

    public void setFecha(Date fecha) {
        this.fecha.setTime(fecha);
    }
    
    public void setFecha(Calendar fecha){
        this.fecha = fecha;
    }
    
    public String getFecha() {
        return ""+fecha.get(Calendar.YEAR)+"-"+(fecha.get(Calendar.MONTH)+1)+"-"+fecha.get(Calendar.DAY_OF_MONTH);
    }
    
    public Calendar getFechaAsCalendar() {
        return fecha;
    }
}
