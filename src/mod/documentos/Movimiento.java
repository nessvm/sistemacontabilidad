/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mod.documentos;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Alan
 */
public class Movimiento {
    private int numP;
    private String tipoP;
    private Calendar fecha;
    private String rfc;
    private int numM;
    private int idCuenta;
    private int idSubcta;
    private String concepto;
    private double importe;

    public void setImporte(double importe) {
        this.importe = importe;
    }
    
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public void setNumM(int numM) {
        this.numM = numM;
    }

    public void setNumP(int numP) {
        this.numP = numP;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public void setTipoP(String tipoP) {
        this.tipoP = tipoP;
    }

    public String getConcepto() {
        return concepto;
    }

    public int getNumM() {
        return numM;
    }

    public int getNumP() {
        return numP;
    }

    public String getRfc() {
        return rfc;
    }

    public double getImporte() {
        return importe;
    }
    
    public String getTipoP() {
        return tipoP;
    }
    
    public void setFecha(int a�o, int mes, int dia) {//En caso de que se quiere asignar una fecha especifica
       fecha = Calendar.getInstance();
       fecha.set(a�o,mes-1,dia);
    }

    public void setFecha(Calendar fecha) {//Para recibir la fecha de la base de datos
        this.fecha = fecha;
    }
    
    public void setFecha(Date fecha){
    	this.fecha.setTime(fecha);
    }
    
    public String getFecha() {//Regresa la fecha en cadena lista para ser insertada en la base
        return ""+fecha.get(Calendar.YEAR)+"-"+(fecha.get(Calendar.MONTH)+1)+"-"+fecha.get(Calendar.DAY_OF_MONTH);
    }

	/**
	 * @return the idCuenta
	 */
	public int getIdCuenta() {
		return idCuenta;
	}

	/**
	 * @return the idSubcta
	 */
	public int getIdSubcta() {
		return idSubcta;
	}

	/**
	 * @param idCuenta the idCuenta to set
	 */
	public void setIdCuenta(int idCuenta) {
		this.idCuenta = idCuenta;
	}
	
	/**
	 * M�todo para la asignaci�n de cuenta y subcuenta con una sola cadena
	 * @param idCuentaSubcuenta El identificador de cuenta y subcuenta a donde va el movimiento con un formato "cccc-s" o "cccc-ss"
	 */
	public void setIdCuenta(String idCuentaSubcuenta) {
		if (idCuentaSubcuenta.length() > 4){
			this.idCuenta = Integer.parseInt(idCuentaSubcuenta.substring(0, 4));
			this.idSubcta = Integer.parseInt(idCuentaSubcuenta.substring(5));
		}
		else
			this.idCuenta = Integer.parseInt(idCuentaSubcuenta);
	}

	/**
	 * @param idSubcta the idSubcta to set
	 */
	public void setIdSubcta(int idSubcta) {
		this.idSubcta = idSubcta;
	}
    
    
}
