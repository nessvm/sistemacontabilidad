package mod.documentos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class Estado {
	private Calendar fecha;
	private ArrayList<Cuenta> activoCirc, activoNoCirc, pasivoCorto,
			pasivoLargo, capitalCont, egresos, ingresos;
	private HashMap<Cuenta, ArrayList<Subcuenta>> indexer;

	/**
	 * Este constructor crea un onjeto de tipo empresa, y acomoda las subcuentas
	 * a las cuentas correspondientes
	 * 
	 * @param cuentas
	 * @param subcuentas
	 */
	public Estado(ArrayList<Cuenta> cuentas, ArrayList<Subcuenta> subcuentas) {
		this.fecha = Calendar.getInstance();
		this.indexer = new HashMap<>();

		ArrayList<Subcuenta> subTemp;

		/*
		 * Se iteran ambas listas, en caso de que los identificadores de cuenta
		 * coincidan, la subcuenta es a�adida a una lista temporal, que despu�s
		 * ser� mapeada con el objeto HashMap y tendr� como llava a la cuenta
		 * padre.
		 */
		for (int i = 0; i < cuentas.size(); i++) {
			subTemp = new ArrayList<>();
			for (int j = 0; j < subcuentas.size(); j++) {
				if (subcuentas.get(j).getIdCuenta() == cuentas.get(i)
						.getIdCta()) {
					subTemp.add(subcuentas.get(j));
				} // end if
			} // end for j
			indexer.put(cuentas.get(i), subTemp);
		} // end for i

		/*
		 * Se iterar� la lista de cuentas, y se verificar� el valor del
		 * idCuenta, y se organizar�n las cuentas en las listas correspondientes
		 * al activo circulante, no circulante, etc.
		 */
		int idCuenta;
		for (int i = 0; i < cuentas.size(); i++){
			idCuenta = cuentas.get(i).getIdCta();
			
			if (idCuenta >= 1100 && idCuenta < 1200){
				activoCirc.add(cuentas.get(i));
				continue;
			}
			else if (idCuenta >= 1200 && idCuenta < 1300){
				activoNoCirc.add(cuentas.get(i));
				continue;
			}
			else if (idCuenta >= 2100 && idCuenta < 2200){
				pasivoCorto.add(cuentas.get(i));
				continue;
			}
			else if (idCuenta >= 2200 && idCuenta < 2300){
				pasivoLargo.add(cuentas.get(i));
				continue;
			}
			else if (idCuenta >= 3000 && idCuenta < 4000){
				capitalCont.add(cuentas.get(i));
				continue;
			}
			else if (idCuenta >= 4000 && idCuenta < 5000){
				egresos.add(cuentas.get(i));
				continue;
			}
			else if (idCuenta >= 5000 && idCuenta < 6000){
				ingresos.add(cuentas.get(i));
				continue;
			}
		}
	}

	/**
	 * @return the fecha
	 */
	public Calendar getFecha() {
		return fecha;
	}

	/**
	 * @return the indexer
	 */
	public HashMap<Cuenta, ArrayList<Subcuenta>> getIndexer() {
		return indexer;
	}

	/**
	 * @return the activoCirc
	 */
	public ArrayList<Cuenta> getActivoCirc() {
		return activoCirc;
	}

	/**
	 * @return the activoNoCirc
	 */
	public ArrayList<Cuenta> getActivoNoCirc() {
		return activoNoCirc;
	}

	/**
	 * @return the pasivoCorto
	 */
	public ArrayList<Cuenta> getPasivoCorto() {
		return pasivoCorto;
	}

	/**
	 * @return the pasivoLargo
	 */
	public ArrayList<Cuenta> getPasivoLargo() {
		return pasivoLargo;
	}

	/**
	 * @return the capitalCont
	 */
	public ArrayList<Cuenta> getCapitalCont() {
		return capitalCont;
	}

	/**
	 * @return the egresos
	 */
	public ArrayList<Cuenta> getEgresos() {
		return egresos;
	}

	/**
	 * @return the ingresos
	 */
	public ArrayList<Cuenta> getIngresos() {
		return ingresos;
	}
	
	

}
