/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mod.documentos;

/**
 *
 * @author Alan
 */
public class Cuenta {
    private int idCta;
    private String clasificacion;
    private String subClasificacion;
    private String nombre;
    private double cargo;
    private double abono;
    
    public Cuenta(){
    	super();
    }

    public Cuenta(int idCta, String clasificacion, String subClasificacion,
			String nombre) {
		super();
		this.idCta = idCta;
		this.clasificacion = clasificacion;
		this.subClasificacion = subClasificacion;
		this.nombre = nombre;
	}

	public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public void setIdCta(int idCta) {
        this.idCta = idCta;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setSubClasificacion(String subClasificacion) {
        this.subClasificacion = subClasificacion;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public int getIdCta() {
        return idCta;
    }

    public String getNombre() {
        return nombre;
    }

    public String getSubClasificacion() {
        return subClasificacion;
    }

	public double getCargo() {
		return cargo;
	}

	public double getAbono() {
		return abono;
	}
	
	public double getSaldo(){
		return abono-cargo;
	}

	/**
	 * @param cargo the cargo to set
	 */
	public void setCargo(double cargo) {
		this.cargo = cargo;
	}

	/**
	 * @param abono the abono to set
	 */
	public void setAbono(double abono) {
		this.abono = abono;
	}
    
}
