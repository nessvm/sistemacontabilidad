
package mod.documentos;

/**
 *
 * @author Alan
 */
public class Subcuenta{
	private int idCuenta;
	private int idSubcta;
	private String nombre;
	private double cargo;
	private double abono;

	public Subcuenta() {
		super();
	}

	public Subcuenta(int idCuenta, int idSubCta, String nombre,
			double cargo, double abono) {
		super();
		this.idCuenta = idCuenta;
		this.idSubcta = idSubCta;
		this.nombre = nombre;
		this.cargo = cargo;
		this.abono = abono;
	}

	/**
	 * @return the idCuenta
	 */
	public int getIdCuenta() {
		return idCuenta;
	}

	/**
	 * @return the idSubCta
	 */
	public int getIdSubcta() {
		return idSubcta;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @return the cargo
	 */
	public double getCargo() {
		return cargo;
	}

	/**
	 * @return the abono
	 */
	public double getAbono() {
		return abono;
	}

	/**
	 * @param idCuenta the idCuenta to set
	 */
	public void setIdCuenta(int idCuenta) {
		this.idCuenta = idCuenta;
	}

	/**
	 * @param idSubCta the idSubCta to set
	 */
	public void setIdSubCta(int idSubCta) {
		this.idSubcta = idSubCta;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getSaldo(){
		return abono-cargo;
	}

	/**
	 * @param idSubcta the idSubcta to set
	 */
	public void setIdSubcta(int idSubcta) {
		this.idSubcta = idSubcta;
	}

	/**
	 * @param cargo the cargo to set
	 */
	public void setCargo(double cargo) {
		this.cargo = cargo;
	}

	/**
	 * @param abono the abono to set
	 */
	public void setAbono(double abono) {
		this.abono = abono;
	}
	
	

}
