

package mod.usuario;

/**
 *
 * @author Alan
 */
public class Usuario {
 private int usr;
 private String tipoU;
 private String nombre;
 private String apPat;
 private String apMat;
 private String contraseña;
 private String email;
    
    public Usuario()
    {
        
    }
    
    public Usuario(int usr, String tipoU, String nombre, String apPat, String apMat, String contraseña, String email)
    {
        this.usr = usr;
        this.tipoU = tipoU;
        this.nombre = nombre;
        this.apPat = apPat;
        this.apMat = apMat;
        this.contraseña = contraseña;
        this.email = email;
    }
 
    public void setApMat(String apMat) {
        this.apMat = apMat;
    }

    public void setApPat(String apPat) {
        this.apPat = apPat;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTipoU(String tipoU) {
        this.tipoU = tipoU;
    }

    public void setUsr(int usr) {
        this.usr = usr;
    }

    public String getApMat() {
        return apMat;
    }

    public String getApPat() {
        return apPat;
    }

    public String getContraseña() {
        return contraseña;
    }

    public String getEmail() {
        return email;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTipoU() {
        return tipoU;
    }

    public int getUsr() {
        return usr;
    }
 
}
