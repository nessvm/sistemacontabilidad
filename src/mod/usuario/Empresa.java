

package mod.usuario;

/**
 *
 * @author Alan
 */
public class Empresa {
    private String rfc;
    private String razonS;
    private int usrCont;
    private String calle;
    private int numExt;
    private int numInt;
    private String colonia;
    private String delegacion;
    private String estado;

    public Empresa()
    {
        
    }
    
    
    public Empresa(String rfc, String razonS, int usrCont, String calle, int numExt, int numInt, String colonia, String delegacion, String estado)
    {
        this.rfc = rfc;
        this.razonS = razonS;
        this.usrCont = usrCont;
        this.calle = calle;
        this.numExt = numExt;
        this.numInt = numInt;
        this.colonia = colonia;
        this.delegacion = delegacion;
        this.estado = estado;
    }
    
    public void setCalle(String calle) {
        this.calle = calle;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public void setDelegacion(String delegacion) {
        this.delegacion = delegacion;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setNumExt(int numExt) {
        this.numExt = numExt;
    }

    public void setNumInt(int numInt) {
        this.numInt = numInt;
    }

    public void setRazonS(String razonS) {
        this.razonS = razonS;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public void setUsrCont(int usrCont) {
        this.usrCont = usrCont;
    }

    public String getCalle() {
        return calle;
    }

    public String getColonia() {
        return colonia;
    }

    public String getDelegacion() {
        return delegacion;
    }

    public String getEstado() {
        return estado;
    }

    public int getNumExt() {
        return numExt;
    }

    public int getNumInt() {
        return numInt;
    }

    public String getRazonS() {
        return razonS;
    }

    public String getRfc() {
        return rfc;
    }

    public int getUsrCont() {
        return usrCont;
    }
}
