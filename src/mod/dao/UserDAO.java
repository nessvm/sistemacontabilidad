package mod.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import mod.usuario.Empresa;

public class UserDAO {
	DataSource ds;

	public UserDAO(DataSource ds) {
		this.ds = ds;
	}

	public boolean verifyPassword(String user, String password) {
		Statement sentencia;
		String query;
		ResultSet result;

		try {
			Connection con = ds.getConnection();
			sentencia = con.createStatement();
			query = String
					.format("SELECT contraseņa FROM Contabilidad.Usuario WHERE usr = %s;",
							user);
			result = sentencia.executeQuery(query);
			if (result.next()) {
				if (result.getString("contraseņa").equals(password)){
					con.close();
					return true;
				}
			} else{
				con.close();
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public String getUserType(String user) {
		Statement sentencia;
		String query;
		ResultSet result;

		try {
			Connection con = ds.getConnection();
			sentencia = con.createStatement();
			query = String
					.format("SELECT tipo FROM Contabilidad.Usuario WHERE usr = %s;",
							user);
			result = sentencia.executeQuery(query);
			if (result.next()){
				con.close();
				return result.getString("tipo");
			}
			else{
				con.close();
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ResultSet getAllUsers() {
		Statement stmnt;
		String query;
		ResultSet result;

		try {
			Connection con = ds.getConnection();
			stmnt = con.createStatement();
			query = String.format("SELECT * FROM Contabilidad.Usuario;");
			result = stmnt.executeQuery(query);
			if (result.next()) {
				return result;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Empresa getUserCompany(String userID){
		Empresa com = new Empresa();
		String query;
		Connection con;
		Statement stmnt;
		ResultSet empresa;
		
		try{
			con = ds.getConnection();
			stmnt = con.createStatement();
			query = String.format("SELECT * FROM Contabilidad.Empresa WHERE usrCont = %s", userID);
			empresa = stmnt.executeQuery(query);
			
			if (empresa.next()){
				if(empresa.getString("usrCont").equals(userID)){
					com.setRazonS(empresa.getString("razonS"));
					com.setUsrCont(empresa.getInt("usrCont"));
					con.close();
					return com;
				}
				else
					con.close();
					return null;
			}
		} catch (SQLException e) {
			System.out.println("El usuario no tiene ninguna empresa registrada");
		} 
		return null;
	}

}
