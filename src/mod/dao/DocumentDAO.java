package mod.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import mod.documentos.Cuenta;
import mod.documentos.Subcuenta;

public class DocumentDAO {
	DataSource ds;

	public DocumentDAO(DataSource ds) {
		this.ds = ds;
	}

	public ArrayList<String> getAccountList() {
		ArrayList<String> accList;
		Connection con;
		Statement stmnt;
		ResultSet result;
		String query;

		accList = new ArrayList<>();
		try {
			con = ds.getConnection();
			stmnt = con.createStatement();
			query = "SELECT idCuenta FROM Contabilidad.Cuenta;";
			result = stmnt.executeQuery(query);

			while (result.next()) {
				accList.add(String.valueOf(result.getInt("idCuenta")));
			}
			con.close();
			return accList;

		} catch (SQLTimeoutException e) {
			System.out
					.println("Se ha excedido el tiempo de respuesta del servidor MySQL");
			return null;

		} catch (SQLException e) {
			System.out
					.println("Ocurri� un error al extraer la lista de cuentas");
			accList.add(String.valueOf(-1));
			accList.add(e.toString());
			return accList;
		}
	}
	
	public ArrayList<String> getSubAccountList() {
		ArrayList<String> accList;
		Connection con;
		Statement stmnt;
		ResultSet result;
		String query;

		accList = new ArrayList<>();
		try {
			con = ds.getConnection();
			stmnt = con.createStatement();
			query = "SELECT idSubCta, idCuenta FROM Contabilidad.Subcuenta;";
			result = stmnt.executeQuery(query);

			while (result.next()) {
				accList.add(result.getString("idCuenta") + "-" + result.getString("idSubCta"));
				
			}
			con.close();
			return accList;

		} catch (SQLTimeoutException e) {
			System.out
					.println("Se ha excedido el tiempo de respuesta del servidor MySQL");
			return null;

		} catch (SQLException e) {
			System.out
					.println("Ocurri� un error al extraer la lista de cuentas");
			accList.add(String.valueOf(-1));
			accList.add(e.toString());
			return accList;
		}
	}
	
	public ArrayList<Cuenta> getAccounts() {
		ArrayList<Cuenta> ctas = new ArrayList<>();
		ArrayList<String> accList = getAccountList();
		GenericCRUD fetcher = new GenericCRUD(ds);
		
		for (int i = 0; i < accList.size(); i++){
			ctas.add((Cuenta)fetcher.selectObject(accList.get(i), "Cuenta"));
		}
		return ctas;
	}
	
	public ArrayList<Subcuenta> getSubaccounts() {
		ArrayList<Subcuenta> ctas = new ArrayList<>();
		ArrayList<String> subaccList = getSubAccountList();
		GenericCRUD fetcher = new GenericCRUD(ds);
		
		for (int i = 0; i < subaccList.size(); i++){
			ctas.add((Subcuenta)fetcher.selectObject(subaccList.get(i), "Subcuenta"));
		}
		return ctas;
	}
	
	public int getCountAccount(String id){
		Connection con;
		int total = 0;
		try {
			con = ds.getConnection();
			Statement stmnt = null;
			ResultSet result = null;
			String query = "";
	        stmnt = con.createStatement();
	        query = "Select count(*) from Contabilidad.Cuenta where id like concat(" + id + ",%);";
	        result=stmnt.executeQuery (query); 
	        result.next(); 
	        total=result.getInt(1); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return total;
	}

}
