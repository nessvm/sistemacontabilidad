
package mod.dao;


import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import mod.documentos.Cuenta;
import mod.documentos.Movimiento;
import mod.documentos.Poliza;
import mod.documentos.Subcuenta;
import mod.usuario.Empresa;
import mod.usuario.Usuario;

/**
 *
 * @author Alan
 */
public class GenericCRUD {
	private Statement sentencia;
	private String query;
	private Empresa emp;
	private Cuenta cuen;
	private Poliza poliza;
	private Movimiento movimiento;
	private DataSource ds;

	public GenericCRUD(DataSource ds){
		this.ds = ds;
	}

	public enum tipo{
		Usuario, Empresa, Cuenta, Subcuenta, Poliza, Movimiento;
	}

	public Object selectObject(String id, String type)
	{
		switch(tipo.valueOf(type))
		{
		case Usuario:
			return selectUser(id);
		case Empresa:
			return selectEmpresa(id);
		case Cuenta:
			return selectCuenta(id);
		case Subcuenta:
			return selectSubcuenta(id);
		case Poliza:
			try {
				return selectPoliza(id);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		case Movimiento:
			try {
				return selectMovimiento(id);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	public String insertObject(Object obj, String type){
		switch(tipo.valueOf(type))
		{
		case Usuario:
			return insertUser((Usuario)obj);
		case Empresa:
			return insertEmpresa((Empresa)obj);
		case Cuenta:
			return insertCuenta((Cuenta)obj);
		case Subcuenta:
			return insertSubcuenta((Subcuenta)obj);
		case Poliza:
			return insertPoliza((Poliza)obj);
		case Movimiento:
			return insertMovimiento((Movimiento)obj);
		}
		return null;        
	}

	public boolean updateObject(Object obj, String type){
		switch(tipo.valueOf(type)){
		case Usuario:
			return updateUser((Usuario)obj);
		case Cuenta:
			return false;
		case Empresa:
			return false;
		case Movimiento:
			return false;
		case Poliza:
			return false;
		default:
			return false;
		}
	}

	//------------------------------------------------------------------------------    
	private Object selectUser(String id)
	{
		try{
			Usuario user = new Usuario();
			Connection conexion = ds.getConnection();
			sentencia = conexion.createStatement();
			query = "select * from Contabilidad.Usuario u where u.usr = '"+id+"';";
			ResultSet resultado = sentencia.executeQuery(query);
			if (resultado.next()){ 
				user.setUsr(resultado.getInt("usr"));
				user.setNombre(resultado.getString("nombre"));
				user.setApPat(resultado.getString("apPat"));
				user.setApMat(resultado.getString("apMat"));
				user.setEmail(resultado.getString("email"));
				user.setContrase�a(resultado.getString("contrase�a"));
				user.setTipoU(resultado.getString("tipoU"));
			}
			else {
				System.out.println("Error al instanciar al usuario: " +id);
				user = null;
			} 
			conexion.close();
			return user;
		}
		catch(SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return null;
		}
		catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de extracci�n");
			e.printStackTrace();
			return null;
		}       
	}

	private String insertUser(Usuario user)
	{
		String usr = null;
		try{
			Connection conexion = ds.getConnection();
			sentencia = conexion.createStatement();
			String query = "INSERT INTO Contabilidad.Usuario (usr, nombre ,apPat , apMat, email, tipo, password) VALUES (default, '"+user.getNombre()+ "','" +user.getApPat() + "', '" +user.getApMat() + "', '" +user.getEmail()+"', '"+user.getTipoU()+"', '"+user.getContrase�a()+"');";
			ResultSet resultado;
			int filaafectada = sentencia.executeUpdate(query);
			if(filaafectada == 1)
			{
				String id="SELECT MAX(id) AS id FROM Contabilidad.usuario;";
				resultado = sentencia.executeQuery(id);
				if(resultado.next())
				{
					usr=""+resultado.getInt("usr");
					conexion.close();
					return usr;  
				}
				else
				{
					System.out.println("Error al sacar el id");
					conexion.close();
					return null;
				}
			}
			else
			{
				System.out.println("Error al insertar al usuario en la BD");
				conexion.close();
				return null;
			}
		}
		catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return null;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de inserci�n");
			e.printStackTrace();
			return null;
		} 
	}

	private Object selectEmpresa(String rfc){
		try{
			emp = new Empresa();
			Connection conexion = ds.getConnection();
			sentencia = conexion.createStatement();
			query = "select * from  Contabilidad.Empresa e, Contabilidad.Usuario u, DomicilioEmp d "+ "where e.rfc = '" + rfc+ "' and u.usr = e.usrCont and e.rfc = d.rfc;";
			ResultSet resultado = sentencia.executeQuery(query);
			if (resultado.next()){ 
				emp.setRfc(resultado.getString("rfc"));
				emp.setRazonS(resultado.getString("razonS"));
				emp.setUsrCont(Integer.parseInt(resultado.getString("usrCont")));
				emp.setCalle(resultado.getString("calle"));
				emp.setNumExt(Integer.parseInt(resultado.getString("numExt")));
				emp.setNumInt(Integer.parseInt(resultado.getString("numInt")));
				emp.setColonia(resultado.getString("colonia"));
				emp.setDelegacion(resultado.getString("delegacion"));
				emp.setEstado(resultado.getString("estado")); 
				conexion.close();
			}
			else {
				System.out.println("Error al instanciar a la empresa: " +rfc);
				emp = null;
				conexion.close();
				return null;
			} 
			return emp;        
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			emp = null;
			return null;
		}
		catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de extracci�n");
			e.printStackTrace();
			emp = null;
			return null;
		}    
	}

	private String insertEmpresa(Empresa emp){
		//String rfc = null;
		try{
			Connection conexion = ds.getConnection();
			sentencia = conexion.createStatement();
			String query="INSERT INTO Contabilidad.Empresa (rfc, razonS, usrCont) VALUES ('"+emp.getRfc()+"','"+emp.getRazonS()+"','"+emp.getUsrCont()+"');";
			int fila = sentencia.executeUpdate(query);
			if(fila == 1){
				String query2 ="INSERT INTO Contabilidad.DomicilioEmp (rfc, calle, numExt, numInt, colonia, delegacion, estado) VALUES ('"+emp.getRfc()+"', '"+emp.getCalle()+"', '"+emp.getNumExt()+"', '"+emp.getNumInt()+"', '"+emp.getColonia()+"', '"+emp.getDelegacion()+"', '"+emp.getEstado()+"');";
				int fila2 = sentencia.executeUpdate(query2);
				if(fila2 == 1){
					conexion.close();
					return emp.getRfc();
				}
				else {
					System.out.println("Error al insertar el domicilio de la empresa en la BD");
					conexion.close();
					return null;
				}
			}
			System.out.println("Error al insertar a la empresa en la BD");
			conexion.close();
			return null;
		}
		catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return null;
		}
		catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de extracci�n");
			e.printStackTrace();
			return null;
		} 
	}
	//------------------------------------------------------------------------------
	private Object selectSubcuenta(String id) {
		try{
			String idCuenta = id.substring(0, 4);
			String idSubCta = id.substring(5);
			Connection con = ds.getConnection();
			Statement stmnt = con.createStatement();
			String query = String.format("SELECT * FROM Contabilidad.Subcuenta WHERE idCuenta = %s and idSubCta = %s", idCuenta, idSubCta);
			ResultSet result = stmnt.executeQuery(query);
			Subcuenta sub = new Subcuenta();

			if (result.next()){
				sub.setIdCuenta(result.getInt("idCuenta"));
				sub.setIdSubCta(result.getInt("IdSubCta"));
				sub.setNombre(result.getString("nombre"));
				sub.setCargo(result.getDouble("cargo"));
				sub.setAbono(result.getDouble("abono"));
				con.close();
				return sub;
			}

			else{
				System.out.println("No fue posible recuperar la subcuenta " + id);
			}
			con.close();

		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
		}
		catch (SQLException e){
			System.out.println("Excepci�n SQL al extraer una Subcuenta");
			e.printStackTrace();
		}  
		return null;
	}

	private Object selectCuenta(String idCuenta){
		try{
			cuen = new Cuenta();
			Connection conexion = ds.getConnection();
			sentencia = conexion.createStatement();
			String query="SELECT * FROM Contabilidad.Cuenta WHERE idCuenta = '"+idCuenta+"';";
			ResultSet resultado = sentencia.executeQuery(query);
			if(resultado.next())
			{
				cuen.setIdCta(resultado.getInt("idCuenta"));
				cuen.setNombre(resultado.getString("nombre"));
				cuen.setClasificacion(resultado.getString("clasificacion"));
				cuen.setSubClasificacion(resultado.getString("subclasificacion"));
			}
			else
			{
				System.out.println("Error al instanciar la cuenta: " +idCuenta);
				conexion.close();
				cuen = null;
				return null;
			}
			conexion.close();
			return cuen;
		}
		catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			cuen = null;
			return null;
		}
		catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de extracci�n");
			e.printStackTrace();
			cuen = null;
			return null;
		}  
	}

	private String insertCuenta(Cuenta cuen){        
		try{
			Connection con = ds.getConnection();
			sentencia = con.createStatement();
			String query = String.format("INSERT INTO `Contabilidad`.`Cuenta` (`idCuenta`, `nombre`, `clasificacion`, `subClasificacion`) VALUES (%s, '%s', '%s', '%s');", cuen.getIdCta(), cuen.getNombre(), cuen.getClasificacion(), cuen.getSubClasificacion());
			int fila = sentencia.executeUpdate(query);

			if(fila == 1) {
				con.close();  
				return "0";
			}
			con.close();
		}
		catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
		}
		catch (SQLException e){
			System.out.println("Excepci�n SQL al insertar la cuenta " + cuen.getIdCta());
			e.printStackTrace();
		}
		return null;
	}

	//---------------------------------------------------------------------
	private String insertSubcuenta(Subcuenta sub) {
		try{
			Connection con = ds.getConnection();
			sentencia = con.createStatement();
			String query = String.format("INSERT INTO `Contabilidad`.`Subcuenta` (`idCuenta`, `idSubCta`, `nombre`) VALUES (%s, %s, '%s');", sub.getIdCuenta(), sub.getIdSubcta(), sub.getNombre());
			int fila = sentencia.executeUpdate(query);

			if(fila == 1) {
				con.close();
				return "0";
			}
			con.close();
		}
		catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
		}
		catch (SQLException e){
			System.out.println("Excepci�n SQL al insertar la subcuenta " + cuen.getIdCta());
			e.printStackTrace();
		}
		return null;
	}

	private Object selectPoliza(String noPoliza) throws ParseException
	{
		try{
			poliza = new Poliza();
			Connection conexion = ds.getConnection();
			sentencia = conexion.createStatement();
			query = "select * from Contabilidad.poliza p "+ "where p.numP = "+noPoliza+";";
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			ResultSet resultado = sentencia.executeQuery(query);
			if (resultado.next()){            
				poliza.setRfc(resultado.getString("rfc"));
				poliza.setFecha((Date)sdf.parse(resultado.getString("fecha")));
				poliza.setTipo(resultado.getString("tipoP"));
				poliza.setDescripcion(resultado.getString("descripcion"));
				poliza.setNumero(resultado.getInt("numP"));
			}
			else {
				System.out.println("Error al instanciar a la poliza: " +noPoliza);
				conexion.close();
				poliza = null;
				return null;
			}
			conexion.close();
			return poliza;
		}
		catch(SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			poliza = null;
			return null;
		}
		catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de extracci�n");
			poliza = null;
			e.printStackTrace();
			return null;
		}
	}    

	private String insertPoliza(Poliza poliza)
	{
		try{
			String noPoliza = null;
			Connection conexion = ds.getConnection();
			sentencia = conexion.createStatement();
			String query = "INSERT INTO Contabilidad.Poliza (numP, tipoP, rfc, fecha, descripcion ) VALUES (default, '"+poliza.getTipo()+ "','" +poliza.getRfc() + "', '"+poliza.getFecha()+"', '"+poliza.getDescripcion()+"');";
			ResultSet resultado;
			int filaafectada = sentencia.executeUpdate(query);
			if(filaafectada == 1)
			{
				String id="SELECT MAX(numP) AS numP FROM Contabilidad.Poliza;";
				resultado = sentencia.executeQuery(id);
				if(resultado.next())
				{
					noPoliza=""+resultado.getInt("numP");
					conexion.close();
					return noPoliza; 
				}
				else
				{
					System.out.println("Error al sacar el numero de poliza");
					conexion.close();
					return null;
				}

			}
			else
			{
				System.out.println("Error al insertar al usuario en la BD");
				conexion.close();
				return null;
			}
		}
		catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return null;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de inserci�n");
			e.printStackTrace();
			return null;
		} 
	}
	//------------------------------------------------------------------------------    
	private Object selectMovimiento(String numM) throws ParseException
	{
		try{
			Connection conexion = ds.getConnection();
			movimiento = new Movimiento();
			sentencia = conexion.createStatement();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
			query = "select * from Contabilidad.Movimiento m "+ "where m.numM = '"+numM+"';";
			ResultSet resultado = sentencia.executeQuery(query);
			if (resultado.next()){ 
				movimiento.setNumM(resultado.getInt("numM"));
				movimiento.setTipoP(resultado.getString("tipoP"));
				movimiento.setRfc(resultado.getString("rfc"));
				movimiento.setNumP(resultado.getInt("numP"));
				movimiento.setFecha((Date) sdf.parse(resultado.getString("fecha")));
				movimiento.setConcepto(resultado.getString("concepto"));
				movimiento.setImporte(resultado.getDouble("importe"));
				movimiento.setIdCuenta(resultado.getInt("idCuenta"));
				movimiento.setIdSubcta(resultado.getInt("idSubCta"));
			}
			else {
				System.out.println("Error al instanciar al movimiento: " + numM);
				movimiento = null;
				conexion.close();
				return null;
			}  
			conexion.close();
			return movimiento;
		}
		catch(SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			movimiento = null;
			return null;
		}
		catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de extracci�n");
			movimiento = null;
			e.printStackTrace();
			return null;
		}
	}    

	private String insertMovimiento(Movimiento movimiento)
	{
		try{
			String numM = null;
			Connection conexion = ds.getConnection();
			sentencia = conexion.createStatement();
			String query = "INSERT INTO Contabilidad.Movimiento (numM, numP, tipoP, rfc, fecha, concepto, idCuenta, idSubCta, importe) VALUES (default, '"+movimiento.getNumP()+"', '"+movimiento.getTipoP()+"', '"+movimiento.getRfc()+"', '"+movimiento.getFecha()+"', '"+movimiento.getConcepto()+"', '"+movimiento.getIdCuenta()+"', '"+movimiento.getIdSubcta() +"', '"+movimiento.getImporte() +"');";
			ResultSet resultado;
			int filaafectada = sentencia.executeUpdate(query);
			if(filaafectada == 1)
			{
				String id="SELECT MAX(numM) AS numM FROM Contabilidad.Movimiento;";
				resultado = sentencia.executeQuery(id);
				if(resultado.next())
				{
					numM=""+resultado.getInt("numM");
					conexion.close();
					return numM;               
				}
				else
				{
					System.out.println("Error al sacar el numero de movimiento");
					conexion.close();
					return null;
				}
			}
			else
			{
				System.out.println("Error al insertar el movimiento en la BD");
				conexion.close();
				return null;
			}
		}
		catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return null;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de inserci�n");
			e.printStackTrace();
			return null;
		} 
	}

	private boolean updateUser(Usuario user) {
		String query;
		Statement stmnt;
		int filasAfectadas;

		try {
			Connection conexion = ds.getConnection();
			stmnt = conexion.createStatement();
			query = String
					.format("UPDATE Contabilidad.Usuario SET nombre='%s' apPat='%s' apMat='%s' contrase�a='%s' email='%s' WHERE usr='%s'",
							user.getNombre(), user.getApPat(), user.getApMat(),
							user.getContrase�a(), user.getEmail());
			filasAfectadas = stmnt.executeUpdate(query);

			if (filasAfectadas == 1){
				System.out.println("Modificados los datos del usuario " + user.getUsr());
				conexion.close();
				return true;
			}

		} catch (SQLTimeoutException e) {
			System.out
			.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return false;
		} catch (SQLException e) {
			System.out.println("Excepci�n SQL con el constructor de inserci�n");
			e.printStackTrace();
			return false;
		}

		return false;
	}

}
