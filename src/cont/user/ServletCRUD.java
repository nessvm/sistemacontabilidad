package cont.user;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mod.dao.GenericCRUD;
import mod.documentos.Cuenta;
import mod.documentos.Subcuenta;
import mod.negocio.RequestFetcher;
import mod.usuario.Usuario;

/**
 * @author Ness creado: 16/11/2013 22:08
 * 
 *         S�lo un administrador est� capacitado para a�adir nuevos contadores o
 *         auxiliares al sistema.
 * 
 *         Y un contador puede a�adir �nicamente nuevos auxiliares.
 * 
 *         Nota: Basarse en el servlet que se uso para el avance de proyecto del
 *         primer departamental
 */
@WebServlet("/ServletCRUD")
public class ServletCRUD extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;

	/**
	 * @see HttpServlet#HttpServlet()
	 * 
	 *      Cuando se instancia el servlet se hace una llamada al pool de
	 *      conexiones, de aqu� saldr�n todas las conexiones a la BD que se
	 *      usar�n a trav�s de todo el sistema
	 */
	public ServletCRUD() {
		super();
		Context initContext;

		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			ds = (DataSource) envContext.lookup("dbPool/Contabilidad");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * @author Ness modificado: 4/12/2013 02:50
	 * 
	 *         De la tabla din�mica de usuarios, en el get se redirije seg�n la
	 *         opci�n que haya escogido el administrador sobre la fila del
	 *         usuario, consultar, modificar o eliminar usuario.
	 * 
	 *         Se obtiene el nombre de usuario
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String option = request.getParameter("lstTipo");
		GenericCRUD register = new GenericCRUD(ds);

		switch(option){
		
		case "Cuenta":
			Cuenta cuenta = new Cuenta();
			cuenta.setNombre(request.getParameter("txtNomCta"));
			cuenta.setClasificacion(request.getParameter("txtClasif"));
			cuenta.setSubClasificacion(request.getParameter("txtSubclasif"));
			cuenta.setIdCta(Integer.parseInt(request.getParameter("txtIdCta")));
			register.insertObject(cuenta, "Cuenta");
			response.sendRedirect("sys/index.html");
			return;
			
		case "Subcuenta":
			Subcuenta subcuenta = new Subcuenta();
			subcuenta.setNombre(request.getParameter("txtNombre"));
			subcuenta.setIdCuenta(Integer.parseInt(request.getParameter("txtIdCta")));
			subcuenta.setIdSubCta(Integer.parseInt(request.getParameter("txtIdSubCta")));
			register.insertObject(subcuenta, "Subcuenta");
			response.sendRedirect("sys/index.html");
			return;
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * @author Ness modificado: 16/11/2013 23:01
	 * 
	 *         Para evitar que la contrase�a que se va a guardar en la base de
	 *         datos viaje en la URL, se utiliza el m�todo POST para el registro
	 *         de nuevos usuarios.
	 * 
	 *         Se debe obtener el objeto representaci�n del usuario en sesi�n, y
	 *         se debe verifi- car que sea un administrador para todos los
	 *         procedimientos para a�adir usuario que no sea un Auxiliar, en
	 *         caso del auxiliar se proceder� sin la verificaci�n, ya que tanto
	 *         administrador como contador har�n uso de �l.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session;
		GenericCRUD courier;
		Usuario nuevo;
		
		session = request.getSession();
		courier = new GenericCRUD(ds);
		
		if (session.getAttribute("usuario") != null) {
			courier = new GenericCRUD(ds);


				if ((nuevo = RequestFetcher.getUserParameters(request)) != null){
					nuevo.setUsr(Integer.parseInt(courier.insertObject(nuevo, "Usuario")));
					response.sendRedirect("sys/index.html");
					System.out.println("Se ha registrado un nuevo usuario " + nuevo.getUsr());
					return;
				}
				else{
					System.out.println("Error con los par�metros de registro");
					response.sendRedirect("sys/newUsers.html");
					return;
			}
		}
	}
}
