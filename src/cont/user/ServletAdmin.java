package cont.user;

import java.io.IOException;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mod.dao.GenericCRUD;
import mod.dao.UserDAO;
import mod.usuario.Usuario;

/**
 * Servlet implementation class ServletAdmin
 */
@WebServlet("/admin/ServletAdmin")
public class ServletAdmin extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAdmin() {
        super();
        Context initContext;
        
		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			ds = (DataSource) envContext.lookup("dbPool/Contabilidad");
		} catch (NamingException e) {
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String option = request.getParameter("op");
		String user = request.getParameter("user");
		GenericCRUD copycat = new GenericCRUD(ds);
		HttpSession session = request.getSession();
		Usuario modUser;
		
		if(request.getSession().getAttribute("adminUser") == null && option == null && user == null){
			UserDAO crowdControl = new UserDAO(ds);
			
			ResultSet result = crowdControl.getAllUsers();
			request.getSession().setAttribute("result", result);
			
			response.sendRedirect("admin_menu.jsp");
		}
		else{
			switch(option){
			
			case "visualizar":
				modUser = (Usuario)copycat.selectObject(user, "Usuario");
				session.setAttribute("user", modUser);
				response.sendRedirect("admin_view.jsp");
				return;
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
