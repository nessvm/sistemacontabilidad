package cont.user;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mod.dao.GenericCRUD;
import mod.usuario.Empresa;
import mod.usuario.Usuario;

/**
 * Servlet implementation class ServletEmpresa
 */
@WebServlet("/ServletEmpresa")
public class ServletEmpresa extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEmpresa() {
        super();
        Context initContext;
        
        try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			ds = (DataSource) envContext.lookup("dbPool/Contabilidad");
		} catch (NamingException e) {
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Usuario user = (Usuario)session.getAttribute("usuario");
		GenericCRUD crud = new GenericCRUD(ds);
		
		Empresa emp = new Empresa();//tengo duda en que mandarle como usrCont al constructor, por eso tiene el 0
		emp.setRazonS(request.getParameter("txtCoName"));
		emp.setRfc(request.getParameter("txtRFC"));
		emp.setCalle(request.getParameter("txtStreet"));
		emp.setNumInt(Integer.parseInt(request.getParameter("txtNint")));
		emp.setNumExt(Integer.parseInt(request.getParameter("txtNext")));
		emp.setColonia(request.getParameter("txtCol"));
		emp.setDelegacion(request.getParameter("txtDel"));
		emp.setEstado(request.getParameter("txtEdo"));
		if (user != null)
			emp.setUsrCont(user.getUsr());
		else{
			System.out.println("Sesi�n expirada");
			response.sendRedirect("index.jsp");
			return;
		}
			
		
		if(crud.insertObject(emp, "Empresa") != null) {
			session.setAttribute("empresa", emp);
			response.sendRedirect("sys/index.html");
			System.out.println("Empresa '" + emp.getRazonS() + "' registrada");
			return;
		}
		else 
			response.sendRedirect("sys/newAccount.html");
			return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
