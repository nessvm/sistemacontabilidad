package cont.user;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mod.dao.GenericCRUD;
import mod.dao.UserDAO;
import mod.usuario.Empresa;
import mod.usuario.Usuario;

/**
 * Servlet implementation class ServletLogin
 */
@WebServlet("/ServletLogin")
public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;

	/**
	 * @see HttpServlet#HttpServlet() Cuando se instancia el servlet se hace una
	 *      llamada al pool de conexiones, de aqu� saldr�n todas las conexiones
	 *      a la BD que se usar�n a trav�s de todo el sistema
	 */
	public ServletLogin() {
		super();
		Context initContext;

		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			ds = (DataSource) envContext.lookup("dbPool/Contabilidad");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * @author Ness creado: 16/11/2013 21:40
	 * 
	 *         Revisar: no es eficiente crear un objeto de cada tipo de usuario
	 *         y verificar si se extrajo correctamente de la BD, �hacer una
	 *         tabla con los nombres de usuario y el tipo que son?
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String user, password;

		HttpSession session;

		UserDAO uDAO;
		GenericCRUD courier;
		Usuario usuario;
		Empresa com;

		user = request.getParameter("txtUser");
		password = request.getParameter("txtPassword");
		session = request.getSession();
		uDAO = new UserDAO(ds);
		courier = new GenericCRUD(ds);

		if (uDAO.verifyPassword(user, password)) {
			usuario = (Usuario) courier.selectObject(user, "Usuario");
			com = uDAO.getUserCompany(user);

			if (usuario.getTipoU().equals("Administrador")) {
				session.setAttribute("admin", usuario);
				System.out.println("Un administrador ha iniciado sesi�n");
				session.setAttribute("empresa", com);
				response.sendRedirect("admin/admin_menu.jsp");
				return;
			}
			else {
				session.setAttribute("usuario", usuario);
				System.out.println("El usuario " + user + " ha iniciado sesi�n");
				response.sendRedirect("sys/index.html");
			}
		}

		else {
			System.out.println("Inicio de sesi�n fallido para el usuario"
					+ user);
		}
	}

}
