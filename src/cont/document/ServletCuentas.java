package cont.document;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mod.documentos.Cuenta;

/**
 * Servlet implementation class ServletCuentas
 */
@WebServlet("/ServletCuentas")
public class ServletCuentas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCuentas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String clasificacion = "";
        String subclasificacion = "";
        String tipo = "";
        String nombre = "";
        String padre = "";
        clasificacion = request.getParameter("lstClasif");
        subclasificacion = request.getParameter("txtSubClasifi");
        tipo = request.getParameter("lstTipo");
        nombre = request.getParameter("txtNomCta");
        padre = request.getParameter("lstDepend");
        String id = "";
        response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<HTML><HEAD><TITLE>Leyendo parámetros</TITLE></HEAD>");
		pw.println("<BODY>");
		pw.println("<p>"+ObtenId(clasificacion, subclasificacion, nombre)+"</p>");
		pw.println("</BODY></HTML>");
		pw.close();
        if(tipo == "Cuenta"){
            id = ObtenId(clasificacion, subclasificacion, nombre);
            //Cuenta cuenta = new Cuenta();
        }else{
            //Subcuenta scuenta = new Subcuenta();
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	public String ObtenId(String clasificacion, String subclasificacion, String nombre){
        String id = "";
        System.out.println(clasificacion);
        System.out.println(subclasificacion);
        if(clasificacion.equals("Activo")){
            id += "1";
            if(subclasificacion.equals("Circulante"))
            	id += "1";
            else if(subclasificacion.equals("Fijo Tangible"))
            	id += "2";
            else if(subclasificacion.equals("Fijo Intangible"))
                id += "3";
            else if(subclasificacion.equals("Otros activos"))
                id += "4";
        }    
        else if(clasificacion.equals("Pasivo")){
            id += "2";
            if(subclasificacion.equals("Corto plazo"))
            	id += "1";
            else if(subclasificacion.equals("Largo Plazo"))
            	id += "2";
        }    
        else if(clasificacion.equals("Capital")){
            id += "3";
            if(subclasificacion.equals("Contribuido"))
            	id += "1";
            else if(subclasificacion.equals("Capital ganado"))
            	id += "2";
        }
        else if(clasificacion.equals("Cuentas complementarias de activo"))
            id += "41";
        else if(clasificacion.equals("Deudora"))
            id += "51";
        else if(clasificacion.equals("Acreedora"))
            id += "61";
        
        //String disponible = Disponible();
        return id;
    }
	
    public String Disponible(){
        String indice = "";
        int i = 0;
       // while(/*Condicion*/){
         //   i++;
        //}
        indice += i;
        return indice;
    }

}
