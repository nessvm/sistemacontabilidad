package cont.document;

import java.io.IOException;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mod.dao.GenericCRUD;
import mod.documentos.Movimiento;
import mod.documentos.Poliza;
import mod.negocio.RequestFetcher;
import mod.usuario.Empresa;

//import mod.documentos.Poliza;

/**
 * Servlet implementation class ServletPoliza
 */
@WebServlet("/ServletPoliza")
public class ServletPoliza extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletPoliza() {
		super();
		Context initContext;

		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			ds = (DataSource) envContext.lookup("dbPool/Contabilidad");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * 
	 *      Se reciben los par�metros de la p�liza, que en su caso ser�n los
	 *      mismos para los movimientos.
	 * 
	 *      De los movimientos se recibe del request �nicamente concepto e
	 *      importe. El n�mero de movimiento ser� asignado al momento de la
	 *      creaci�n, el sistema es el que llevar� el orden de asignaci�n y el
	 *      usuario ve el n�mero de movimiento una vez que la p�liza fue creada.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Movimiento> movs;
		GenericCRUD courier;
		HttpSession session = request.getSession();
		Poliza poliza;
		Empresa com;
		
		courier = new GenericCRUD(ds);
		if ((session.getAttribute("usuario")) == null){
			System.out.println("La sesi�n ha expirado");
			response.sendRedirect("index.jsp");
			return;
		}
		
		com = (Empresa)courier.selectObject("GALL123456XXX", "Empresa");
		session.setAttribute("empresa", com);
		poliza = RequestFetcher.getPolizaParameters(request);
		poliza.setNumero(Integer.parseInt(courier.insertObject(poliza, "Poliza")));
		movs = RequestFetcher.getAllMovParameters(request, poliza);

		for (int i = 0; i < movs.size(); i++) {
			courier.insertObject(movs.get(i), "Movimiento");
			System.out.println("Movimiento " + movs.get(i).getNumM() + " registrado");
		}
		System.out.println("P�liza " + poliza.getNumero() + " registrada");
		response.sendRedirect("sys/index.html");
	}
}
