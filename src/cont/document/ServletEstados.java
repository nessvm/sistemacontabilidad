package cont.document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mod.negocio.StreamDocument;
import mod.negocio.XML;
import mod.usuario.Empresa;

/**
 * Servlet implementation class ServletEstados
 */
@WebServlet("/ServletEstados")
public class ServletEstados extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DataSource ds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEstados() {
        super();
        Context initContext;

		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			ds = (DataSource) envContext.lookup("dbPool/Contabilidad");
		} catch (NamingException e) {
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println(request.getServletContext().getContextPath());
		FileOutputStream xml_file = new FileOutputStream(new File("D:/workspace/SistemaContabilidad/WebContent/cuentas.xml"));
		
		XML xml = new XML(ds, xml_file);
		
		Empresa empresa = new Empresa("Empresa", "razon", 1, "1", 1, 1, "San san", "uno", "Mexico");
		xml.doXML(empresa);
		InputStream xmlStream = request.getServletContext().getResourceAsStream("/cuentas.xml");
		OutputStream htmlStream = response.getOutputStream();
		InputStream xslStream = request.getServletContext().getResourceAsStream("/balance.xsl");
		
		if (request.getParameter("op") != null) {
			xslStream = request.getServletContext().getResourceAsStream("/balance.xsl");
		}
		else{
			xslStream = request.getServletContext().getResourceAsStream("/resultados.xsl");
		}
		StreamDocument.transform(xmlStream, xslStream, htmlStream);
		/*
		 * Aqu� se usar� XSL para transformar el estado de resultados de XML a HTML
		 */
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
