<!DOCTYPE html>
<!-- Update 20/11/13 21:20 -->
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Inicio de Sesi�n</title>
		<link rel="stylesheet" type="text/css" href="js//css//style.css">
		<link rel="stylesheet" type="text/css" href="js//css//style_menu.css">
		
		<script type="text/javascript" src="js//js//jquery.js"></script>
		<script type="text/javascript" src="js//js//script_functions.js"></script>
	</head>
	<body>
		<header>	
			<div id="Encabezado"><img alt="logo" src="img//logo.jpg" width="15%" height="15%"></div>
			<div class="contenedorMenu"></div>
		</header>		
	
		<section class="tabla" id="divSession">		
			<div id="Marco"></div>
			<div id="Centro">		
				<form action="ServletLogin" method="post">
					<table id="tblAuthe">
						<tr>
							<td> <label for="txtUser">Usuario: </label></td>
							<td> <input id="txtUser" name="txtUser" class="valNume" type="text" 
							required="required" placeholder="Usuario"> </td>
						</tr>
						<tr>
							<td> <label for="txtPassword">Contrase�a: </label></td>
							<td> <input id="txtPassword" name="txtPassword" type="password" 
							required="required" placeholder="Contrase�a" ></td>
						</tr>					
					</table>
					<input id="btnEntrar" name="btnEntrar" type="submit" value="Entrar">
					<input id="btnRecoverPass" name="btnRecoverPass" 
					type="button" value="Recuperar Contrase�a" 
					onclick="JavaScript:window.location='recoverPass.html'">
					<a href="recoverUser.html"><input id="btnRecoverUser" name="btnRecoverUser" 
					type="button" value="Recuperar Usuario"
					onclick="JavaScript:window.location='recoverUser.html'"></a>
					<!-- Qu� es mejor usar como html o como jsp, si uso como html se 
					necesita tener hipervinculo 
					<a href="recoverUser.html"><input id="btnRecoverUser" name="btnRecoverUser" 
					type="button" value="Recuperar Usuario"
					onclick="JavaScript:window.location='recoverUser.html'"></a>-->
				</form>
			</div>

		</section>			

		<footer>
			<label>Desarrollo creativo </label>
		</footer>
	</body>
</html>