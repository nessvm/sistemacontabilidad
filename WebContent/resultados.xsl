<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Estado">
		<html>
			<head>
				<meta charset="ISO-8859-1"/>
				<title>Estado de resultados integral</title>
				<link rel="stylesheet" type="text/css" href="js/css/theme.css"/>
			</head>
			
			<body>
				<header class="titulo">
					<h1><xsl:value-of select="@empresa"/></h1>
					<h2>Estado de resultados integral al <xsl:value-of select="@dia"/> de <xsl:value-of select="@mes"/> del <xsl:value-of select="@año"/></h2>
				</header>
				
				<article class="container">
				
					<section class="resultados">
						<table>
							<colgroup>
								<col class="cuenta"/>
								<col class="saldo"/>
							</colgroup>
							<tr align="center">
								<th>Deudoras</th>
							</tr>
							<xsl:for-each select="ResultadosDeudoras/Cuenta">
							<tr>
								<td><xsl:value-of select="@id"/><xsl:value-of select="@nombre"/></td>
								<td></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>					
							</xsl:for-each>
							<tr align="center">
								<th>Acreedoras</th>
							</tr>
							<xsl:for-each select="ResultadosAcreedoras/Cuenta">
							<tr>
								<td><xsl:value-of select="@id"/><xsl:value-of select="@nombre"/></td>
								<td></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>							
							</xsl:for-each>
						</table>
					</section>
					
				</article>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>