function insertInputRow(){
	var fieldSet = document.getElementById("fdsMovimientos");
	var count = document.getElementById("count").getAttribute("value");
	
	var div = document.createElement("div");
		div.setAttribute("id", "divMovi" + count);
		div.setAttribute("name", "divMovi" + count);
		div.setAttribute("class", "Movimiento");
	
	var newAccountN = document.createElement("input");
		newAccountN.setAttribute("id", "txtAccountN" + count);
		newAccountN.setAttribute("name", "txtAccountN" + count);
		newAccountN.setAttribute("class", "txtAccountN");		
		newAccountN.setAttribute("type", "text");
		newAccountN.setAttribute("required", "");
		newAccountN.setAttribute("placeholder", "Numero de Cuenta");	
		
	var newConcept = document.createElement("input");
		newConcept.setAttribute("id", "txtConcept" + count);
		newConcept.setAttribute("name", "txtConcept" + count);
		newConcept.setAttribute("class", "txtConcept");
		newConcept.setAttribute("type", "text");
		newConcept.setAttribute("required", "");
		newConcept.setAttribute("placeholder", "Concepto");
		
	var newDebe = document.createElement("input");
		newDebe.setAttribute("id", "txtDebe" + count);
		newDebe.setAttribute("name", "txtDebe" + count);
		newDebe.setAttribute("class", "Debe");
		newDebe.setAttribute("type", "text");
		newDebe.setAttribute("required", "");
		newDebe.setAttribute("placeholder", "Debe");
		//newDebe.setAttribute("onkeypress", "return suma()");
		
	var newHaber = document.createElement("input");
		newHaber.setAttribute("id", "txtHaber" + count);
		newHaber.setAttribute("name", "txtHaber" + count);
		newHaber.setAttribute("class", "Haber");
		newHaber.setAttribute("type", "text");
		newHaber.setAttribute("required", "");
		newHaber.setAttribute("placeholder", "Haber");
		newHaber.setAttribute("onkeypress", "return validarNumeros()");
		
	fieldSet.appendChild(document.createElement("br"));
	fieldSet.appendChild(div);
	
	div.appendChild(newAccountN);
	div.appendChild(newConcept);
	div.appendChild(newDebe);
	div.appendChild(newHaber);
	document.getElementById("count").setAttribute("value", ++count);
}