<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Estado">
		<html>
			<head>
				<meta charset="ISO-8859-1"/>
				<title>Balance General</title>
				<link rel="stylesheet" type="text/css" href="js/css/theme.css"/>
			</head>
			
			<body>
				<header class="titulo">
					<h1><xsl:value-of select="@empresa"/></h1>
					<h2>Balance General al <xsl:value-of select="@dia"/> de <xsl:value-of select="@mes"/> del <xsl:value-of select="@año"/></h2>
				</header>
				
				<article class="container">
				
					<section class="activo">
						<table>
							<colgroup>
								<col class="cuenta"/>
								<col class="saldo"/>
							</colgroup>
							<tr align="center">
								<th>Activo</th>
								<th><xsl:value-of select="Activo/@suma"></xsl:value-of></th>
							</tr>
							<tr align="left">
								<th>Circulante</th>
								<th><xsl:value-of select="Activo/Circulante/@suma"></xsl:value-of></th>
							</tr>
							<xsl:for-each select="Activo/Circulante/Cuenta">
							<tr>
								<td><xsl:value-of select="@id"/><xsl:value-of select="@nombre"/></td>
								<td></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							<xsl:for-each select="Subcuenta">
							<tr>
								<td><xsl:value-of select="@id"/></td>
								<td><xsl:value-of select="@nombre"></xsl:value-of></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							</xsl:for-each>
							</xsl:for-each>
							<tr align="left">
								<th>No circulante</th>
								<th><xsl:value-of select="Activo/NoCirculante/@suma"></xsl:value-of></th>
							</tr>
							<xsl:for-each select="Activo/NoCirculante/Cuenta">
							<tr>
								<td><xsl:value-of select="@id"/><xsl:value-of select="@nombre"/></td>
								<td></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							<xsl:for-each select="Subcuenta">
							<tr>
								<td><xsl:value-of select="@id"/></td>
								<td><xsl:value-of select="@nombre"></xsl:value-of></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							</xsl:for-each>
							</xsl:for-each>
						</table>
					</section>
					
					<section class="pasivo">
						<table>
							<colgroup>
								<col class="cuenta"/>
								<col class="saldo"/>
							</colgroup>
							<tr align="center">
								<th>Pasivo</th>
								<th><xsl:value-of select="Pasivo/@suma"></xsl:value-of></th>
							</tr>
							<tr align="left">
								<th>A corto plazo</th>
								<th><xsl:value-of select="Pasivo/CortoPlazo/@suma"></xsl:value-of></th>
							</tr>
							<xsl:for-each select="Pasivo/CortoPlazo/Cuenta">
							<tr>
								<td><xsl:value-of select="@id"/><xsl:value-of select="@nombre"/></td>
								<td></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							<xsl:for-each select="Subcuenta">
							<tr>
								<td><xsl:value-of select="@id"/></td>
								<td><xsl:value-of select="@nombre"></xsl:value-of></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							</xsl:for-each>
							</xsl:for-each>
							<tr align="left">
								<th>A largo plazo</th>
								<th><xsl:value-of select="Pasivo/LargoPlazo/@suma"></xsl:value-of></th>
							</tr>
							<xsl:for-each select="Pasivo/LargoPlazo/Cuenta">
							<tr>
								<td><xsl:value-of select="@id"/><xsl:value-of select="@nombre"/></td>
								<td></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							<xsl:for-each select="Subcuenta">
							<tr>
								<td><xsl:value-of select="@id"/></td>
								<td><xsl:value-of select="@nombre"></xsl:value-of></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							</xsl:for-each>
							</xsl:for-each>
							<tr align="center">
								<th>Capital contable</th>
							</tr>
							<xsl:for-each select="CapitalContable/Cuenta">
							<tr>
								<td><xsl:value-of select="@id"/> <xsl:value-of select="@nombre"/></td>
								<td></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							<xsl:for-each select="Subcuenta">
							<tr>
								<td><xsl:value-of select="@id"/></td>
								<td><xsl:value-of select="@nombre"></xsl:value-of></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							</xsl:for-each>
							</xsl:for-each>
							
							<tr align="center">
								<th>Complemento del activo</th>
							</tr>
							<xsl:for-each select="ComplementoActivo/Cuenta">
							<tr>
								<td><xsl:value-of select="@id"/><xsl:value-of select="@nombre"/></td>
								<td></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							<xsl:for-each select="Subcuenta">
							<tr>
								<td><xsl:value-of select="@id"/></td>
								<td><xsl:value-of select="@nombre"></xsl:value-of></td>
								<td><xsl:value-of select="@saldo"></xsl:value-of></td>
							</tr>
							</xsl:for-each>
							</xsl:for-each>
						</table>
					</section>
				</article>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>