<!DOCTYPE html>
<!-- Update 21/11/13 18:04 -->
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>P�liza</title>
		<!-- Para calendario -->
		<link href="..//js/css/calendario.css" type="text/css" rel="stylesheet">
		<script src="..//js/js/calendar.js" type="text/javascript"></script>
		<script src="..//js/js/calendar-es.js" type="text/javascript"></script>
		<script src="..//js/js/calendar-setup.js" type="text/javascript"></script>
		<!-- Estilos generales -->
		<link rel="stylesheet" type="text/css" href="..//js//css//style.css">
		<link rel="stylesheet" type="text/css" href="..//js//css//style_menu.css">
		
		<script src="..//js//js//jquery.js"></script>
		<script src="..//js//js//script_functions.js"></script>
		
		<link rel="stylesheet" type="text/css" href="..//js//css//styles.css">
		<script src="..//js/js/functions.js" type="text/javascript"></script>
		<script src="..//js/js/generator.js"></script>		
	</head>
	<body>
		<header>
			<div id="Encabezado"><img alt="logo" src="..//img//logo.jpg" width="15%" height="15%"></div>
			<div class="contenedorMenu"></div>
		</header>
		<section>
			<% request.getSession().setAttribute("lblSdoDebe", "0.00");
			request.getSession().setAttribute("lblSdoHaber", "0.00");	%>
					
			<div id="Marco"></div>
			<div id="Centro">
				<form action="../ServletPoliza" method="post">
					<div class="divCenter">
								<table id="tblHeaderP" class="tblPoliza">
									<tr>
										<td>
											<label for="lstTypeP">Tipo de p�liza:</label>
											<select id="lstTypeP" name="lstTypeP">
												<option id="itmDiario" value="Diario">Diario</option>
												<option id="itmIngreso" value="Ingreso">Ingreso</option>
												<option id="itmEgreso" value="Egreso">Egreso</option>
										</select>
										</td>
										<td>
											<label for="txtNumPoliza">N�mero de p�liza:</label>
											<input id="txtNumPoliza" name="txtNumPoliza" type="text" 
											required="required" placeholder="Numero de poliza" 
											onkeypress="validarNumeros()">
										</td>
										<td>
											<label for="txtDescription">Descripci�n:</label>
											<input id="txtDescription" name="txtDescription" type="text" 
											required="required" placeholder="Descripcion">
										</td>
										<td>
											<label for="txtDate">Fecha:</label>
											<input id="txtDate" name="txtDate" type="text" 
											required="required" value="dd-mm-aaaa">
											<img src="..//img/calendario.png" width="16" height="16" border="0" 
											title="Fecha Inicial" id="btnlanzador">
												<script type="text/javascript"> 
												   Calendar.setup({ 
												    inputField     :    "txtDate",    
												     ifFormat     :     "%d-%m-%Y",     
												     button     :    "btnlanzador"      
												}); 
												</script>
										</td>
									</tr>
								</table>
								<br>
								<br>
								<table id="tblEnPoliza" class="tblPoliza"> 
									<tr>
										<td class="txtAccountN"><label>N�mero de cuenta: </label></td>
										<td class="txtConcept"><label>Concepto: </label></td>
										<td class="txtImporte"><label>Debe: </label></td>
										<td class="txtImporte"><label>Haber: </label></td>
									</tr>
								</table>
								<fieldset id="fdsMovimientos" class="tblPoliza">
									<input id="count" value="1" hidden="true"/>
									<!-- falta validar cuenta  -->
									<!-- falta el llenado automatico de 000000 -->
									<!-- si no se tiene valor en debe o haber no se permitir� 
									crear un nuevo movimiento -->
								</fieldset>
								<button id="btnAgregar" onclick="insertInputRow()">Agregar Movimiento</button>
								<label for="lblSdoDebe">Saldo deudor: </label>
								<label id="lblSdoDebe">0.00</label>
								<script type="text/javascript">
										
								</script>
								<label for="lblSdoHaber">Saldo acreedor: </label>
								<label id="lblSdoHaber"><%=request.getSession().getAttribute("lblSdoHaber")%></label>
								<br>
								<label for="lblSdo">Saldo: </label>		
								<label id="lblSdo"><!-- Deudor menos acreedor --></label>
								<br>
								<br>						
								<input id="btnGuardar" name="btnGuardar" type="submit" value="Guardar p�liza">
								<input id="btnCancelar" name="btnCancelar" type="button" 
								value="Cancelar" onclick="JavaScript:window.location='index.html'"> <!-- Falta redireccionar -->
								<input id="btnSalir" name="btnSalir" type="button" 
								value="Salir" onclick="JavaScript:window.location='index.html'"> <!-- falta redireccionar -->
							</div>
				</form>
			</div>
		</section>
		<footer>
		</footer>	
	</body>
</html>