<%@package  mod.usuario; %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Ver perfil</title>
		<link rel="stylesheet" type="text/css" href="..//js//css//style.css">
		<link rel="stylesheet" type="text/css" href="..//js//css//style_menu.css">
		
		<script type="text/javascript" src="..//js//js//jquery.js"></script>
		<script type="text/javascript" src="..//js//js//script_functions.js"></script>
		<script>
		request.getSession().setAttribute("usuario").setNombre();
			$(document).ready(function(){
				$('#frmProfile').load(){
					var a = request.getSession().getAttribute("usuario").getNombre().val();	 
					$('#txtName').val(a);
				};
			});
		</script>
	</head>
	<body>
		<header>
			<div id="Encabezado"><img alt="logo" src="..//img//logo.jpg" width="15%" height="15%"></div>
			<div class="contenedorMenu"></div>
		</header>
		<section>			
			<div id="Marco"></div>
			<div id="Centro">
			</div>
			<form id="frmProfile" action="" method="get">
				<table id="tblProfile">
					<tr>
						<td><label for="txtName">Nombre:</label></td>
						<td><input id="txtName" name="txtName"></td>
					</tr>
					<tr>
						<td><label for="txtApPat">Apellido Paterno:</label></td>
						<td><input id="txtApPat" name="txtApPat" type="text"></td>
					</tr>
					<tr>
						<td><label for="txtApMat">Apellido Materno:</label></td>
						<td><input id="txtApMat" name="txtApMat" type="text"></td>
					</tr>
					<tr>
						<td><label for="txtEmail">Correo electronico:</label></td>
						<td><input id="txtEmail" name="txtEmail" type="text"></td>
					</tr>
				</table>				
			</form>
		</section>
		<footer>
			<label>Desarrollo</label>
		</footer>
	</body>
</html>