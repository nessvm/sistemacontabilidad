<!DOCTYPE html>
<!-- act 131001 -->
<%@page import="java.sql.ResultSet"%>
<html>
	<head>
<meta charset="ISO-8859-1">
<title>Men� de Administrador</title>
</head>
	<body>
		<header>
		<div id = "Encabezado" align="center" style="color:navy; background: white;">
		<table style="size: landscape; text-align: center;">
				<tr>
					<td> <img alt="LogoIPn" src="img/logo_ipn.jpg" height="50" width="50"></td>
					<td width="500"> <h1> Gesti�n de usuarios </h1> </td>
					<td> <img alt="LogoEscom" src="img/images.jpg" height="50" width="50"></td>
				</tr>
			</table>  
		</div>	
		</header>
		
		<%	if(request.getSession().getAttribute("result") == null)
				response.sendRedirect("ServletAdmin"); %>
		
		<div id = "Menu" style="float: left; height: 460px; width: 100px; background: #2A8E82; 
		font-family: sans-serif; font-style: oblique;">
		</div>
		
		<div id = "table" align="center" style="height: 460px; text-transform:capitalize;">
			<table style="font-family: sans-serif;">
				<tr>
					<td><strong>Usuario</strong></td>
					<td><strong>Correo</strong></td>
					<td><strong>Nombre</strong></td>
					<td><strong>Apellido paterno</strong></td>
					<td><strong>Apellido materno</strong></td>
					<td><strong>Gestionar</strong></td>
				</tr>
				
				<%	ResultSet result = (ResultSet)request.getSession().getAttribute("result");
					if(result != null && result.first())
						while(result.next()){ %>
				<tr>
					<td><small><%= result.getString("usr") %> </small></td>
					<td><small><%= result.getString("email") %></small></td>
					<td><small><%= result.getString("nombre") %></small></td>
					<td><small><%= result.getString("apPat") %></small></td>
					<td><small><%= result.getString("apMat") %></small></td>
					<td>
					<a href="ServletAdmin?op=visualizar&&user=
						<%= result.getString("usr") %>">
						<img src="img/spyglass.png" width="22" height="22" id="visualizar">
					</a>
					<a href="ServletAdmin?op=editar&&user=
						<%= result.getString("usr") %>">
						<img src="img/pencil.png" width="22" height="22" id="modificar" >
					</a>
					<a href="ServletAdmin?op=eliminar&&user=
						<%= result.getString("usr") %>">
						<img src="img/delete.jpg" width="22" height="22" id="eliminar" >
					</a>
					</td>
				</tr>
				<% }
					request.getSession().removeAttribute("result");%>
				
			</table>
		</div>		
		
		
		<footer>
		<div id="Pie" align="center" style="color:white; background: navy; text-align: center;
			font-size: x-small;">
			<br>Escuela Superior de C�mputo.
			<br> Desarrollado por: Ben�tez Rodriguez Tania y N�stor Vel�zquez Macin.
		</div>
		</footer>
	</body>
</html>