<!-- act 131001 -->
<%@page import="mod.usuario.Usuario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
 <%@page import="java.sql.ResultSet"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modificar información de usuario</title>
</head>
<body>
	<header>
		<div id = "Encabezado" align="center" style="color:navy; background: white;">
			<table style="size: landscape; text-align: center;">
				<tr>
					<td> <img alt="LogoIPn" src="img/logo_ipn.jpg" height="50" width="50"></td>
					<td width="500"> <h1> <%= request.getSession().getAttribute("adminUser") %> </h1> </td>
					<td> <img alt="LogoEscom" src="img/images.jpg" height="50" width="50"></td>
				</tr>
			</table>  
		</div>
		<script>
		/////////////////////////////////////////// 
					function ValidarForm()
					{
						var usr, pw, name, apPat, apMat, email;
						usr = document.getElementById("txtUser").value;
						pw = document.getElementById("txtPassword").value;
						name = document.getElementById("txtName").value;
						apPat = document.getElementById("txtApPat").value;
						apMat = document.getElementById("txtApMat").value;
						email = document.getElementById("txtEmail").value;
											 
						if(usr.length == 0 || /^\s+$/.test(usr) || 
								pw.length == 0 || /^\s+$/.test(pw) || 
							name.length == 0 || /^\s+$/.test(name) ||  
							apPat.length == 0 || /^\s+$/.test(apPat) || 
							apMat.length == 0 || /^\s+$/.test(apMat) ||
							email.length == 0 || /^\s+$/.test(email) )					
						{
							alert("No puedes dejar campos vacios");
							return false; 
						}
						else if(nCaract(email, '.') != 1 || nCaract(email, '@' ) != 1 
								|| posCar(email, '@') > posCar(email, '.'))
						{						
							alert("Ingresa un correo que sea valido");
							return false;
						}
						else 
						{
							var resp=confirm("¿Estás seguro de actualizar?");
							if (resp == true)
							{
							  	return true;
							}
							else
							 {
							  	return false;
							 }
							return true;
						}					
					}
					
					function nCaract(cadena, caract)
					{
						var n;
						n = cadena.split(caract).length - 1;
						return n;
					}
					
					function posCar(cadena, caract)
					{
						var n;
						n = cadena.indexOf(caract);
						return n;	
					}
				</script>	
	</header>		
		
	<div id = "Menu" style="float: left; height: 460px; width: 100px; background: #2A8E82; 
		font-family: sans-serif; font-style: oblique;">
		<br>
		<button onclick="Javascript:window.location='admin_menu.jsp'">Regresar</button>
		<br>
		<br>
		<button onclick="Javascript:window.location='actualizar.jsp'">Actualizar datos</button>
	</div>
	
	<div id = "info" align="center" style="height: 460px; text-transform:capitalize;">
	<form action="ServletAdmin" method="post" name="frmModifica" onsubmit="return ValidarForm()">
		<table>
		<%	Usuario user = (Usuario)session.getAttribute("user");
					if(user != null) { %>
		<tr>
			<td><label for="user">ID Usuario:</label></td>
			<td><input readonly name="user" value=<%= user.getUsr() %>></td>
		</tr>
		<tr>
			<td><label for="user">Contraseña:</label></td>
			<td><input name="pwd" value=<%= user.getContraseña() %>></td>
		</tr>
		<tr>
			<td><label for="email">Correo:</label></td>
			<td><input name="email" value=<%= user.getEmail() %>></td>
		</tr>
		<tr>
			<td><label for="name">Nombre:</label></td>
			<td><input name="name" value=<%= user.getNombre() %>></td>
		</tr>
		<tr>
			<td><label for="apPat">Apellido Paterno:</label></td>
			<td><input name="apPat" value=<%= user.getApPat() %>></td>
		</tr>
		<tr>
			<td><label for="user">Apellido Materno:</label></td>
			<td><input name="apMat" value=<%= user.getApMat() %>></td>
		</tr>
		<% }
		%>
		</table>
		<input name="btnEnviar" type="submit" value="Enviar"/>
		<button type="button" onclick="Javascript:window.location='admin_menu.jsp'">Cancelar</button>
		
		
		</form>
	</div>
	
</body>
</html>