<%@page import="mod.usuario.Usuario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!-- act 131001 -->
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Información de usuario</title>
</head>
<body>
	<header>
		<div id = "Encabezado" align="center" style="color:navy; background: white;">
			<table style="size: landscape; text-align: center;">
				<tr>
					<td> <img alt="LogoIPn" src="img/logo_ipn.jpg" height="50" width="50"></td>
					<td width="500">  </td>
					<td> <img alt="LogoEscom" src="img/images.jpg" height="50" width="50"></td>
				</tr>
			</table>  
		</div>	
	</header>		
		
	<div id = "Menu" style="float: left; height: 460px; width: 100px; background: #2A8E82; 
		font-family: sans-serif; font-style: oblique;">
		<br>
		<button onclick="Javascript:window.location='admin_menu.jsp'">Gestión de usuarios</button>
		<br>
		<br>
		<button onclick="Javascript:window.location='actualizar.jsp'">Actualizar datos</button>
	</div>
	
	<div id = "info" align="center" style="height: 460px; text-transform:capitalize; font-family: sans-serif;">
		<table>
		<%	Usuario user = (Usuario)session.getAttribute("user");
					if(user != null) { %>
		<tr>
			<td><label for="user">ID Usuario:</label></td>
			<td><input readonly="readonly" name="user" value=<%= user.getUsr() %>></td>
		</tr>
		<tr>
			<td><label for="user">Contraseña:</label></td>
			<td><input readonly="readonly" name="pwd" value=<%= user.getContraseña() %>></td>
		</tr>
		<tr>
			<td><label for="email">Correo:</label></td>
			<td><input readonly="readonly" name="email" value=<%= user.getEmail() %>></td>
		</tr>
		<tr>
			<td><label for="name">Nombre:</label></td>
			<td><input readonly="readonly" name="name" value=<%= user.getNombre() %>></td>
		</tr>
		<tr>
			<td><label for="apPat">Apellido Paterno:</label></td>
			<td><input readonly="readonly" name="apPat" value=<%= user.getApPat() %>></td>
		</tr>
		<tr>
			<td><label for="user">Apellido Materno:</label></td>
			<td><input readonly="readonly" name="user" value=<%= user.getApMat() %>></td>
		</tr>
		<% }
		%>
		</table>
		
		<button type="button" onclick="Javascript:window.location='admin_modify.jsp'">Modificar</button>
		<button type="button" onclick="Javascript:window.location='admin_menu.jsp'">Regresar</button>
	</div>
</body>
</html>